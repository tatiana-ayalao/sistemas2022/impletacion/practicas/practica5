﻿USE alumno;

INSERT INTO alumno 
  (expediente, nombre, apellidos, fechaNac)VALUES 
  (1000,       'Maria', 'Gomez','1994/09/30'),
  (1001,       'Carlos','Marin','1997/02/14'),
  (1002,       'David','Hernandez','1998/01/31');

SELECT * FROM alumno a;

INSERT INTO esdelegado (expedienteDelegado, expedienteAlumno)VALUES 
                         (1002, 1001),
                         (1002, 1000);

SELECT * FROM esdelegado e;

INSERT INTO modulo 
 (codigo, nombre) VALUES 
 (20, 'BaseDatos'),
 (21, 'CMS'),
 (22, 'SQL');

SELECT * FROM modulo m;


INSERT INTO cursa (expedienteAlumno, codigoModulo) VALUES
                       (1000, 20),
                       (1001, 21),
                       (1002, 22);
SELECT * FROM cursa c;




INSERT INTO imparte 
  (dniProfesor, codigoModulo)VALUES 
  (7228, 20),
  (7330, 21),
  (7331, 22);

SELECT * FROM imparte i;

INSERT INTO profesor 
  (dni, nombre, direccion, tfno) VALUES 
  (7228,'Clara','Calle Cisneros', 9423031),
  (7330,'Ramon','Calle Madrid', 9423030),
  (7331,'Manuel','Calle Simancas', 9423056);

SELECT * FROM profesor p;


