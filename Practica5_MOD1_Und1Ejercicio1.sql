﻿DROP DATABASE IF EXISTS alumno; 
CREATE DATABASE alumno;
USE alumno;

CREATE TABLE alumno (
expediente int,
nombre varchar(100),
apellidos varchar(100),
fechaNac date,
PRIMARY KEY (expediente)
);

CREATE TABLE EsDelegado (
expedienteDelegado int,
  expedienteAlumno int,
PRIMARY KEY (expedienteDelegado, expedienteAlumno),
UNIQUE KEY (expedienteAlumno)
);

CREATE TABLE cursa (
expedienteAlumno int,
codigoModulo int,
PRIMARY KEY (expedienteAlumno, codigoModulo)
);

CREATE TABLE modulo (
codigo int,
nombre varchar(100),
PRIMARY KEY (codigo)
);

CREATE TABLE imparte (
dniProfesor int,
codigoModulo int,
PRIMARY KEY (dniProfesor, codigoModulo),
UNIQUE KEY (codigoModulo)
);

CREATE TABLE profesor ( 
dni int,
nombre varchar(100),
direccion varchar(100),
tfno int
);

ALTER TABLE EsDelegado 
ADD CONSTRAINT fkesDelegadoAlumno FOREIGN KEY (expedienteDelegado) REFERENCES alumno (expediente) ON DELETE RESTRICT ON UPDATE RESTRICT,
ADD CONSTRAINT fkesDelegadoAlumno1 FOREIGN KEY (expedienteAlumno) REFERENCES alumno (expediente) ON DELETE RESTRICT ON UPDATE RESTRICT;


ALTER TABLE cursa
ADD CONSTRAINT fkcursaAlumno FOREIGN KEY (expedienteAlumno) REFERENCES alumno (expediente) ON DELETE RESTRICT ON UPDATE RESTRICT,
ADD CONSTRAINT fkcursaModulo FOREIGN KEY (codigoModulo) REFERENCES modulo (codigo) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE imparte
ADD CONSTRAINT fimparteProfesor FOREIGN KEY (dniProfesor) REFERENCES profesor (dni) ON DELETE RESTRICT ON UPDATE RESTRICT,
ADD CONSTRAINT fkimparteModulo FOREIGN KEY (codigoModulo) REFERENCES modulo (codigo) ON DELETE RESTRICT ON UPDATE RESTRICT;


